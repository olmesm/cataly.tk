import allCapitalcase from '@/filters/all/capitalcase';
import allLowercase from '@/filters/all/lowercase';
import capitalize from '@/filters/capitalize';
import caseCamel from '@/filters/case/camel';
import caseKebab from '@/filters/case/kebab';
import casePascal from '@/filters/case/pascal';
import caseSnake from '@/filters/case/snake';
import stripLetter from '@/filters/strip/letter';
import stripNumber from '@/filters/strip/number';
// import underscoreAfter from '@/filters/underscore/after';
// import underscoreBefore from '@/filters/underscore/before';
// import underscoreEncapsulate from '@/filters/underscore/encapsulate';

export default {
  allCapitalcase,
  allLowercase,
  capitalize,
  caseCamel,
  caseKebab,
  casePascal,
  caseSnake,
  stripLetter,
  stripNumber,
  // underscoreAfter,
  // underscoreBefore,
  // underscoreEncapsulate,
};
