export default str => (typeof str === 'string' ? str.toLowerCase() : null);
