export default str => (typeof str === 'string' ? str.toUpperCase() : null);
