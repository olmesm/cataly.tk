export default str => (typeof str !== 'string' ? str :
  str.replace(/[0-9]/g, '')
);
