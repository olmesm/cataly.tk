export default str => (typeof str !== 'string' ? str :
  str.replace(/[A-z]/g, '')
);
