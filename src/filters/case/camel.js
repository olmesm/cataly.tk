import pascalCase from '@/filters/case/pascal';

function lowerFirstLetter(word) {
  return `${word[0].toLowerCase()}${word.slice(1)}`;
}

export default str => (
  typeof str !== 'string' ? str :
    lowerFirstLetter(pascalCase(str))
);
