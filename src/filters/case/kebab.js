export default str => (
  typeof str !== 'string' ? str :
    str.split(/-|\s/g)
      .join('-')
      .toLowerCase()
);
