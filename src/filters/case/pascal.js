import capitalize from '@/filters/capitalize';

export default str => (
  typeof str !== 'string' ? str :
    str.split(/-|\s/g)
      .map(capitalize)
      .join('')
);
