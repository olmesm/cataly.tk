export default str => (
  typeof str !== 'string' ? str :
    str.replace(/\s/g, '_')
      .toLowerCase()
);
