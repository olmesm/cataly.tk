export default (word) => {
  if (typeof word === 'string' && !!word) {
    return `${word[0].toUpperCase()}${word.slice(1).toLowerCase()}`;
  }

  if (typeof word === 'number') {
    return word;
  }

  return null;
};
