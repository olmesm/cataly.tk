import numberStrip from '@/filters/strip/number';
import TEST_STRING from '../../../test-string';

const numberStripped = 'ABCDEF- Lorem ipsum dolor sit amet';

describe('Filter: numberStripped', () => {
  it('string is numberStripped', () => {
    expect(numberStrip(TEST_STRING)).to.equal(numberStripped);
  });

  it('numbers are returned as null', () => {
    const TEST_NUMBER = 1234;

    expect(numberStrip(TEST_NUMBER)).to.equal(TEST_NUMBER);
  });

  it('undefined to return input', () => {
    expect(numberStrip(undefined)).to.equal(undefined);
  });

  it('null to return input', () => {
    expect(numberStrip(null)).to.equal(null);
  });
});
