import letterStrip from '@/filters/strip/letter';
import TEST_STRING from '../../../test-string';

const letterStripped = '-123     ';

describe('Filter: letterStripped', () => {
  it('string is letterStripped', () => {
    expect(letterStrip(TEST_STRING)).to.equal(letterStripped);
  });

  it('letters are returned as null', () => {
    const TEST_NUMBER = 1234;

    expect(letterStrip(TEST_NUMBER)).to.equal(TEST_NUMBER);
  });

  it('undefined to return input', () => {
    expect(letterStrip(undefined)).to.equal(undefined);
  });

  it('null to return input', () => {
    expect(letterStrip(null)).to.equal(null);
  });
});
