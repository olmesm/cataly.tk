import capitalize from '@/filters/capitalize';

const TEST_STRING = 'lorem ipsum dolor sit';
const EXPECTED_STRING = 'Lorem ipsum dolor sit';

describe('Filter: Capitalize', () => {
  it('it capitalizes any string', () => {
    expect(capitalize(TEST_STRING)).to.equal(EXPECTED_STRING);
  });

  it('it returns the input if number', () => {
    const number = 1234;

    expect(capitalize(number)).to.equal(number);
  });

  it('it returns null if undefined or null', () => {
    let testInput = null;
    expect(capitalize(testInput)).to.equal(null);

    testInput = undefined;
    expect(capitalize(testInput)).to.equal(null);
  });
});
