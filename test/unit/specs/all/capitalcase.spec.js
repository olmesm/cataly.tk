import caps from '@/filters/all/capitalcase';
import TEST_STRING from '../../../test-string';

describe('Filter: Caps', () => {
  it('it capitalizes any string', () => {
    expect(caps(TEST_STRING)).to.equal(TEST_STRING.toUpperCase());
  });

  it('it returns null if number', () => {
    const number = 1234;

    expect(caps(number)).to.equal(null);
  });

  it('it returns null if undefined or null', () => {
    let testInput = null;
    expect(caps(testInput)).to.equal(null);

    testInput = undefined;
    expect(caps(testInput)).to.equal(null);
  });
});
