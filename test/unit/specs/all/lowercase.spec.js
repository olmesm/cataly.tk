import lower from '@/filters/all/lowercase';
import TEST_STRING from '../../../test-string';

describe('Filter: Lower', () => {
  it('it lowercases any string', () => {
    expect(lower(TEST_STRING)).to.equal(TEST_STRING.toLowerCase());
  });

  it('it returns null if number', () => {
    const number = 1234;

    expect(lower(number)).to.equal(null);
  });

  it('it returns null if undefined or null', () => {
    let testInput = null;
    expect(lower(testInput)).to.equal(null);

    testInput = undefined;
    expect(lower(testInput)).to.equal(null);
  });
});
