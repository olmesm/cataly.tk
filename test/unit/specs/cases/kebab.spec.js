import kebab from '@/filters/case/kebab';
import TEST_STRING from '../../../test-string';

const kebabCased = 'abcdef-123-lorem-ipsum-dolor-sit-amet';

describe('Filter: Kebab', () => {
  it('string is kebab cased', () => {
    expect(kebab(TEST_STRING)).to.equal(kebabCased);
  });

  it('numbers are returned as is', () => {
    const TEST_NUMBER = 1234;

    expect(kebab(TEST_NUMBER)).to.equal(TEST_NUMBER);
  });

  it('undefined to return input', () => {
    expect(kebab(undefined)).to.equal(undefined);
  });

  it('null to return input', () => {
    expect(kebab(null)).to.equal(null);
  });
});
