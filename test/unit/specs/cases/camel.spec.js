import camel from '@/filters/case/camel';
import TEST_STRING from '../../../test-string';

const camelCased = 'abcdef123LoremIpsumDolorSitAmet';

describe('Filter: Camel', () => {
  it('string is camelcased', () => {
    expect(camel(TEST_STRING)).to.equal(camelCased);
  });

  it('numbers are returned as is', () => {
    const TEST_NUMBER = 1234;

    expect(camel(TEST_NUMBER)).to.equal(TEST_NUMBER);
  });

  it('undefined to return input', () => {
    expect(camel(undefined)).to.equal(undefined);
  });

  it('null to return input', () => {
    expect(camel(null)).to.equal(null);
  });
});
