import pascal from '@/filters/case/pascal';
import TEST_STRING from '../../../test-string';

const pascalCased = 'Abcdef123LoremIpsumDolorSitAmet';

describe('Filter: pascal', () => {
  it('string is pascalcased', () => {
    expect(pascal(TEST_STRING)).to.equal(pascalCased);
  });

  it('numbers are returned as is', () => {
    const TEST_NUMBER = 1234;

    expect(pascal(TEST_NUMBER)).to.equal(TEST_NUMBER);
  });

  it('undefined to return input', () => {
    expect(pascal(undefined)).to.equal(undefined);
  });

  it('null to return input', () => {
    expect(pascal(null)).to.equal(null);
  });
});
