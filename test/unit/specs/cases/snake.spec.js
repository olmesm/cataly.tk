import snake from '@/filters/case/snake';
import TEST_STRING from '../../../test-string';

const snakeCased = 'abcdef-123_lorem_ipsum_dolor_sit_amet';

describe('Filter: Snake', () => {
  it('string is snakecased', () => {
    expect(snake(TEST_STRING)).to.equal(snakeCased);
  });

  it('numbers are returned as is', () => {
    const TEST_NUMBER = 1234;

    expect(snake(TEST_NUMBER)).to.equal(TEST_NUMBER);
  });

  it('undefined to return input', () => {
    expect(snake(undefined)).to.equal(undefined);
  });

  it('null to return input', () => {
    expect(snake(null)).to.equal(null);
  });
});
