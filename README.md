# [Cataly.tk](http://cataly.tk)

Just String tools for now.

## To Run

```sh
yarn run
```

## Built with

- Gitlab CI & Gitlab Pages
- Vue.js
- flexbox

## Features

## Resources

* [Preview your website with Gitlab CI and Surge](https://medium.com/jubianchi-fr/preview-your-website-with-gitlab-ci-and-surge-5b861d7c9a3a)

<!--# cataly.tk

> tools for speeding up dev

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).-->
